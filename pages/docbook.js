import React from 'react';
import Link from 'next/link';
import Router from 'next/router';

import axios from 'axios';
import _ from 'lodash';
import is from 'is_js';

import Layout from '../components/templates/Layout';
import ContentMenu from '../components/organisms/doc/ContentMenu';

export default class extends React.Component {
    static async getInitialProps(props) {
        console.log('getInitialProps --->> ', props);
        return {test:555};
      }   
    render() {
        return (
            <Layout title={ 'NOMINUM - Documents - Manpage' }>
            <div className='row'>
                <div id="content" className="content">
                    <div className="help-page"><h2 className="margin-bottom-medium">Help and Documentation</h2>
                    <div className="help-page-layout row-flex">
                        <article className="content">
                            <h1 id="general" className="text-info">General</h1>
                            <p>
                            This section includes answers to common questions about using the Nominum Applications
                            Portal and to common questions about component management using the Nominum Applications Portal, found
                            underneath the gear icon.
                            </p>
                        </article>
                    </div>
                    </div>
                </div>
            </div>
            </Layout>
        )
    }
}