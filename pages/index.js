import React from 'react';
import Link from 'next/link';
import Router from 'next/router';
import _ from 'lodash';
import is from 'is_js';

import Layout from '../components/templates/Layout';
import Search from '../components/organisms/Search';

const handler = (val) => {
  let query = {};
  if (val.products.length > 0) {
    query.products = val.products;
  }
  if (val.versions.length > 0) {
    query.versions = val.versions;
  }
  if (val.query.length > 0) {
    query.query = val.query;
  }
  Router.push({
    pathname: '/search',
    query: {...query,doctype:'html',page:1}
  })}

const search = {
    products : ['Vantio CacheServe','Subscriber Services','Subscriber Portal'],
      versions : [],
      prodVer: {
          'Vantio CacheServe': [
              '17.1',
              '17.2',
              '17.1.1'
          ],
          'Subscriber Services': [
              '17.1',
              '17.1.1',
              '15.4.1'
          ],
          'Subscriber Portal': [
              '17.2',
              '17.1.1',
              '16.2'
          ]
      },
      searchForm: {
          products: [],
          versions: [],
          query: ''
      }
};

export default class extends React.Component {
  constructor(props) {
    super(props);
    console.log('----', Router);
    this.state = {
      search : {
        products : props.products || [],
        versions : [],
        prodVer: props.prodVer || {},
        searchForm: {
          products: [],
          versions: [],
          query: ''
        }
      }
    }
  }

  static async getInitialProps(props) {
    return {prodVer:props.query.prodVer, products: _.keys(props.query.prodVer)};
  }

  render() {
    return (
      <Layout title={ 'NOMINUM - Documents...' }>
        <Search 
          {...this.state.search} 
          handleSearch={handler}
        ></Search>
      </Layout>
    );
  }
}
