import React from 'react';
import { If, Then, Else } from 'react-if';
import { Pagination } from 'react-bootstrap';
import Link from 'next/link';
import Router from 'next/router';

import axios from 'axios';
import _ from 'lodash';
import is from 'is_js';

import Layout from '../components/templates/Layout';
import Search from '../components/organisms/Search';
import SearchResults from '../components/organisms/SearchResults';

class SearchPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      empty: false,
      query: '',
      artists: [],
      limit: 5,
      page: 1,
      total: -1,
      search: {
        products_ : ['Vantio CacheServe','Subscriber Services','Subscriber Portal'],
        products : props.products || [],
        versions : [],
        prodVer_: {
            'Vantio CacheServe': [
                '17.1',
                '17.2',
                '17.1.1'
            ],
            'Subscriber Services': [
                '17.1',
                '17.1.1',
                '15.4.1'
            ],
            'Subscriber Portal': [
                '17.2',
                '17.1.1',
                '16.2'
            ]
        },
        prodVer: props.prodVer || {},
        type: 'html',
        searchForm: {
            products: [],
            versions: [],
            query: ''
        },
      },
      SearchResults: {
        total: 0,
        data: []
      }
    };

    let query = props.url.query;
    if (is.array(query.products)) {
       let versions = [];
       this.state.search.searchForm.products = query.products;
       query.products.forEach(element => {
            versions = [...versions, ...this.state.search.prodVer[element]];
        });
        this.state.search.versions = _.uniq(versions);
    } else if (is.string(query.products)) {
       this.state.search.searchForm.products = [query.products];
       this.state.search.versions = [...this.state.search.prodVer[query.products]];
    }
    
    if (is.array(query.versions)) {
       this.state.search.searchForm.versions = query.versions;
    } else if (is.string(query.versions)) {
      this.state.search.searchForm.versions = [query.versions];
    }
    if (is.string(query.query)) {
      this.state.search.searchForm.query = query.query;
    }
    this.state.search.type = query.doctype;
    this.state.page = query.page;
    console.log(props)
    this.handleSearch = this.handleSearch.bind(this);
    this.handleDocTypeChange = this.handleDocTypeChange.bind(this);
    this.handlePageChange = this.handlePageChange.bind(this);
    this.handlePerPageChange = this.handlePerPageChange.bind(this);
    this.fetchData = this.fetchData.bind(this);
    this.componentWillMount = this.componentWillMount.bind(this);
    this.componentDidUpdate = this.componentDidUpdate.bind(this);
  }

  static async getInitialProps(props) {
    console.log('getInitialProps --->> ', props.query);
    let products = [];
    //for (let key of props.query.prodVer) {
    //  products.push(key);
    //}
    if (!props.query.prodVer) {
      try {
        let response = await axios.get('/api/prod-ver');
        return {prodVer:response.data.prodVer, products: _.keys(response.data.prodVer)}
      } catch (e) {
        return {}
      }
      
    } else {
      return {prodVer:props.query.prodVer, products: _.keys(props.query.prodVer)};
    }
  }

  onProductListChange() {
    
  }

  componentWillMount() {
    console.log('componentWillMount---!!!!');
    this.fetchData();
  }

  

  componentDidUpdate() {
    console.log('componentDidUpdate---!!!!',this.state);
  }


  fetchData() {
    this.setState({loading: true}, () => {
        console.log('fetchData',this.state.search.searchForm);
        axios.get('/api/search', {
        params: {
          ...this.state.search.searchForm,
          doctype: this.state.search.type,
          limit: this.state.limit,
          page: this.state.page
        }
      })
      .then((response) => {
        console.log('resp-->>>', response)
        this.setState({SearchResults:response.data});
      })
      .catch((error) => {
        console.log('resp-err-->>>', error)
        this.setState({loading: false, SearchResults:{total:0,data:[]}});
      });
    });
  }

  handleSearch(searchObj) {
      console.log('search is: ', searchObj);
      if (searchObj.products && searchObj.products.length == 0) {
        delete searchObj.products;
      }
      if (searchObj.versions && searchObj.versions.length == 0) {
        delete searchObj.versions;
      }
      Router.push({
        pathname: '/search',
        query: { 
          ...searchObj,
          doctype: 'html',
          page:1
        }
      });
      this.setState({page:1,search:{...this.state.search, searchForm: searchObj}});
      this.fetchData();

  }

  handlePageChange(page) {
    console.log('Page-->', page);
    let searchObj = {...this.state.search.searchForm};
    if (searchObj.products && searchObj.products.length == 0) {
      delete searchObj.products;
    }
    if (searchObj.versions && searchObj.versions.length == 0) {
      delete searchObj.versions;
    }
    Router.push({
      pathname: '/search',
      query: { 
        ...searchObj,
        doctype: this.state.search.type,
        page:page
      }
    });
    this.setState({page:page});
    this.fetchData();
  }

  handlePerPageChange() {

  }

  handleDocTypeChange(docType) {
    let searchObj = {...this.state.search.searchForm};
    if (searchObj.products && searchObj.products.length == 0) {
      delete searchObj.products;
    }
    if (searchObj.versions && searchObj.versions.length == 0) {
      delete searchObj.versions;
    }
    this.setState({page:1,search: {...this.state.search,type:docType}});
    console.log('...', searchObj);
    Router.push({
      pathname: '/search',
      query: { 
        ...searchObj,
        doctype: docType,
        page:1
      }
    });
    this.fetchData();
  }

  //componentWillMount() {
  //  console.log('componentWillMount', this.props);
  //}

  render() {
    return (
      <Layout title={ 'NOMINUM - Documents - Search' }>
        <Search {...this.state.search} handleSearch={this.handleSearch} total={this.state.SearchResults.total}></Search>
        <SearchResults {...this.state.SearchResults} 
          page={this.state.page} 
          limit={this.state.limit} 
          type={this.state.search.type} 
          handleDocTypeChange={this.handleDocTypeChange}
          handlePageChange={this.handlePageChange}
          handlePerPageChange={this.handlePerPageChange}
        >dfg</SearchResults>
      </Layout>
    );
  }
}

export default SearchPage;
