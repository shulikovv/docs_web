import React from 'react';
import Link from 'next/link';

import Layout from '../../components/templates/Layout';
import Search from '../../components/organisms/Search';
export default () => {
  return (
    <Layout title={ 'NOMINUM - admin - html' }>
      <Search></Search>
    </Layout>
  );
}
