import React from 'react';
import Link from 'next/link';
import Router from 'next/router';

import qs from 'qs';
import axios from 'axios';
import _ from 'lodash';
import is from 'is_js';

import Layout from '../components/templates/Layout';
import ContentMenu from '../components/organisms/doc/ContentMenu';

export default class extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        disabled: true
      }
    }
    static async getInitialProps(props) {
        //console.log('getInitialProps --->> ', props.query.doc);
        if (!props.req) {
          let doc = await axios.get('/api/doc/', {
            params: props.query,
            paramsSerializer: (params) => qs.stringify(params, { arrayFormat: 'brackets' })
          });
          return {doc: doc.data.doc};
        } else {
          return {doc: props.query.doc};
        }
      }   
    render() {
       console.log('ppp--->',this.props);
       let {html} = this.props.doc;
        return (
          <Layout title={ 'NOMINUM - Documents - Document' }>
          <div className='row'>
          <div id="content" className="content">
          <div className="help-page"><h2 className="margin-bottom-medium">Help and Documentation</h2>
            <div className="help-page-layout row-flex">
              <aside className="sidebar">
                <div className="sidenav">
                  <div className="search search--empty">
                    <div className="input-group">
                      <input 
                        className="form-control search__input" 
                        placeholder="Search" 
                        type="search" 
                      />
                    </div>
                    <div className="alert alert-warning hidden">Nothing matches your search</div>
                  </div>
                  <ContentMenu {...this.props.doc}></ContentMenu>
                </div>
              </aside>
              <article className="content">{/* region GENERAL HELP */}
               <div className='row'>
                <div className='col-xs-12 col-md-12' dangerouslySetInnerHTML={{__html: html || ''}}/>
               </div>
              </article>
            </div>
          </div>
        </div>
        
                </div>
          </Layout>
        )
    }
}