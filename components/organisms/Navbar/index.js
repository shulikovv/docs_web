import React from 'react';

class Navbar extends React.Component {
    render() {
        return (
            <header id="mainNav">
                <div className="nom-nav-container">
                    <div id="subnav" className="nom-navbar" data-id="nav-NPS">
                        <div className="subnav nom-navbar">
                            <ul className="nom-navigation">
                                    <li
                                    className="campaignsPage planCampaignPage composeCampaignPage assignCampaignPage reviewCampaignPage">
                                    <a href="/">Home</a>
                                </li>

                                <div className="nav-right">

                                    <li className="adminPage">
                                        <a href="">Admin</a>
                                    </li>

                                </div>
                            </ul>
                        </div>
                    </div>
                    <div className="divider"></div>
                    {/*--start icons for mobile menu from nom-header-container--*/}
                    <div className="nom-navbar hidden-md hidden-lg">
                        <ul className="nom-navigation">

                            <li className="dropdown">
                                <a data-toggle="dropdown">
                                    <i className="fa fa-cog"></i>
                                    <span>Settings</span>
                                    <i className="fa fa-caret-down fa-xs"></i>
                                </a>
                                <ul className="dropdown-menu">
                                    <li>
                                        <a href="#app/components/engines">Engines</a>
                                    </li>
                                    <li>
                                        <a href="#app/components/engineGroups">Engine Groups</a>
                                    </li>

                                    <li>
                                        <a href="#app/components/deviceGroups">Device Groups</a>
                                    </li>

                                    <li>
                                        <a href="#app/components/policyManagers">Policy Managers</a>
                                    </li>
                                    <li>
                                        <a href="#app/components/logicalClusters">Logical Clusters</a>
                                    </li>
                                    <li>
                                        <a href="#app/components/users">User Management</a>
                                    </li>
                                    <li>
                                        <a href="#app/components/settings">Settings</a>
                                    </li>
                                </ul>
                            </li>

                            <li data-id="logout" className="home-icon accountEdit">
                                <a>
                                    <i className="fa fa-user" aria-hidden="true"></i>
                                    <span>Account Settings</span>
                                </a>
                            </li>
                            <li data-id="logout" className="home-icon logout-btn">
                                <a>
                                    <i className="fa fa-sign-out" aria-hidden="true"></i>
                                    <span>Logout</span>
                                </a>
                            </li>

                        </ul>
                    </div>
                    {/*--end--*/}
                </div>
            </header>
        )
    }
}

export default Navbar;