import React from 'react';
import Link from 'next/link';
import Alink from '../../../atoms/Alink';
export default class extends React.Component {
    constructor(props) {
      super(props);
      this.generateMenu = this.generateMenu.bind(this);
    }

    generateMenu() {
      return this.props.menu.map((element,index) => {
        return (
          <li><a href={element.h} key={index}>{element.t}</a></li>
        )
      });
    }

    render() {
        
        return(
            <ul className="nav outline" role="menu">
                    {this.props.menu.map((element,index) => {
                      return (
                        <li key={index}>
                        <Link 
                          href={{ 
                            pathname: '/doc', 
                            query: { docId:this.props.source.docId,h:element.h },
                            //asPath: '/doc/'+this.props.source.docId+'/'+element.h
                          }} 

                          as={'/doc/'+this.props.source.docId+element.h}
                          replace
                        ><a>{element.t}</a></Link></li>
                      )
                    })}
            </ul>
        );
    }
}