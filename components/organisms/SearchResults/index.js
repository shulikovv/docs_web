import React from 'react';
import Link from 'next/link';
import is from 'is_js';
import _ from 'lodash';


import Pagination from '../../molecules/Pagination';
import FoundDoc from '../../molecules/FoundDoc';
import {Tabs, Pane} from '../../atoms/Tabs';

class SearchResults extends React.Component {
    constructor(props) {
        super(props);
        console.log('SearchResults', props);

        this.state = {
			key: 1
		};

        this.componentDidUpdate = this.componentDidUpdate.bind(this);
        this.generateList = this.generateList.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
    }

    componentDidUpdate(prevProps, prevState, prevContext) {
        console.log('SearchResults - componentDidUpdate',this.props);
    }

    componentWillUpdate(nextProps) {

    }

    generateList() {
        return _.map(this.props.data, (document)=>{
            return (
                <FoundDoc document={document} key={document._id}></FoundDoc>
            )
        })
    }

    handleSelect(key) {
        this.props.handleDocTypeChange(key);
		//alert(`selected ${key}`);
		this.setState({ key });
	}

    render() {
        console.log('total --->', this.props.total,Math.ceil(this.props.total/10));
        return (
            <div>
                <div className='row'>
                    <div className='col-xs-12 col-md-12'>
                        <div className='nom-card margin-vertical-small'> 
                        <Tabs selected={0} onSelect={this.handleSelect}>
                            <Pane label="PRODUCT DOCUMENTATION" key='html'></Pane>
                            <Pane label="OTHER DOCUMENTATION" key='other'></Pane>
                        </Tabs>
                        <div className='nom-tab-panel'>
                            <div className="nom-tab-content">
                                <div role="tabpanel" className="nom-tab-pane active">
                                    {this.generateList()}
                                    <Pagination 
                                        total={Math.ceil(this.props.total/10)} 
                                        current={this.props.page}
                                        onChange={this.props.handlePageChange}
                                        onPerPageChange={this.props.handlePerPageChange}
                                    ></Pagination>   
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}    

export default SearchResults;