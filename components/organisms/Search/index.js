import React from 'react';
import Link from 'next/link';
import Select2 from 'react-select2-wrapper';
import Select from '../../atoms/Select';
import is from 'is_js';
import _ from 'lodash';

class Search extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            products : props.products,
            versions : props.versions,
            prodVer: props.prodVer,
            searchForm: props.searchForm
        };

        this.onProductSelectChanged = this.onProductSelectChanged.bind(this);
        this.onVersionsSelectChanged = this.onVersionsSelectChanged.bind(this);
        this.onQueryChanged = this.onQueryChanged.bind(this);
        this.clickSearch = this.clickSearch.bind(this);
        this.clickClearAll = this.clickClearAll.bind(this);
    }

    componentDidMount() {
        //$('select').select2({
        //    width: 'resolve',
        //    minimumResultsForSearch: 'Infinity'
        //});
    }

    onProductSelectChanged(val) {
        let versions = [];
        if(is.array(val)) {
            val.forEach(element => {
                versions = [...versions, ...this.state.prodVer[element]];
            });
        }
        versions = _.uniq(versions);
        this.setState({versions:versions, searchForm: { ...this.state.searchForm, products: val}});
    }

    onVersionsSelectChanged(val) {
        this.setState({searchForm: { ...this.state.searchForm, versions: val}});
    }

    onQueryChanged(e) {
        this.setState({searchForm: { ...this.state.searchForm, query: e.target.value}});
    }

    clickSearch() {
        this.props.handleSearch(this.state.searchForm);
    }

    clickClearAll() {
        this.setState({versions:[],searchForm: {
            products: [],
            versions: [],
            query: ''
        }});
    }

    render() {
        let value01 = [];
        return (
            <div>
                <div className='row'>
                    <div className='col-xs-12 col-md-12'>
                    <div className='nom-card margin-vertical-small'>
                        <div className='card-header'>
                            Search documentation
                        </div>
                        <div className='card-content'>
                        <div className='form-group'>
                            <div className='row form-group'>
                                <div className='col-xs-10 col-md-11'>
                                    <input type="text" className="form-control" onChange={this.onQueryChanged} value={this.state.searchForm.query}/>
                                </div>
                                <div className=''>
                                    <button className="nom-btn-primary" onClick={this.clickSearch}><i className="fa fa-search"></i></button>
                                </div>
                            </div>
                            <div className='row form-group'>
                                <div className='col-xs-10 col-md-11'>
                                    <div className='row'>
                                        <div className='col-xs-6 col-md-6'>
                                        <Select
                                            value={this.state.searchForm.products}
                                            multiple={true}
                                            data={this.state.products}
                                            placeholder='All products'
                                            onChange={(e)=>{
                                                this.onProductSelectChanged(e);
                                            }}
                                        />         
                                        </div>
                                        <div className='col-xs-6 col-md-6'>
                                        <Select
                                            value={this.state.searchForm.versions}
                                            multiple={true}
                                            data={this.state.versions}
                                            placeholder='All versions'
                                            onChange={(e)=>{
                                                this.onVersionsSelectChanged(e);
                                            }}
                                        />                       
                                        </div>
                                    </div>
                                </div>
                                <div className=''>
                                    <a className='' style={{lineHeight:"34px"}} onClick={this.clickClearAll}>Clear All</a>
                                </div>
                            </div>
                            <hr/>
                        </div>
                            <p>Your search returned {this.props.total} results</p>
                        </div>
                    </div>
                    </div>
                </div>            
            </div>
        )
    }
    
}

export default Search;