export default ({ children }) => (
    <footer className="footer nom-container">
        <div className="modal fade" id="terms-page" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h2 id="myModalLabel" className="modal-title">"Terms"</h2>
                    </div>
                    <div className="modal-body"></div>
                    <div className="modal-footer"></div>
                </div>
            </div>
        </div>

        <div className="container">
            <div className="copyright">
                <p>17.1.1.0.ci <a href="http://nominum.com/" target="_blank">Nominum,
                    Inc.</a> All Rights Reserved.
                    <a className="license" data-toggle="modal" data-target="#terms-page" data-backdrop="static">Terms &amp;
                        Conditions</a>.
                    Dates and times are in America/Los_Angeles unless otherwise indicated.
                </p>
            </div>
        </div>
    </footer>
);