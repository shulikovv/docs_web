import React from 'react';

class Header extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="nom-header-container">
            <div className="header" id="header">
                <button
                    type="button"
                    className="side-menu-btn pull-left hidden-md hidden-lg"
                    aria-expanded="false">
                    <i className="fa fa-bars large" aria-hidden="true"></i>
                </button>
                <a className="brand" href="/" alt="logo" title="Nominum">
                    <img src="/static/images/logo_header.svg"/>
                </a>
                <h2 className="header-title">Documentation</h2>
                <div className="pull-right succor-icons">
                    <a href="" className="help">
                        <i className="fa fa-question-circle"></i>
                    </a>
                    <div className="inline dropdown hidden-xs hidden-sm">
                        <a data-toggle="dropdown" className="dropdown-toggle">
                            <i className="fa fa-cog"></i>
                        </a>
                        <ul className="dropdown-menu margin-top-small dropdown-menu-right"></ul>
                    </div>
                    <div className="nom-user-nav inline dropdown hidden-xs hidden-sm">
                        <button
                            className="btn-link dropdown-toggle"
                            type="button"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="true">
                            <span id="loggedInUsername">Username</span>
                            <span className="caret"></span>
                        </button>
                        <ul className="dropdown-menu margin-top-small dropdown-menu-right">
                            <li data-id="logout">
                                <a className="accountEdit">Account Settings</a>
                            </li>
                            <li>
                                <a href="">Scheduled Reports</a>
                            </li>
                            <li data-id="logout">
                                <a className="logout-btn">Logout</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        )
    }
}

export default Header;