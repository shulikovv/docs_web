export default ({ children }) => (
    <header className="breadcrumbs-container">
        <div className="container">
            <div className="nom-breadcrumbs">
                <ol className="nom-breadcrumb"></ol>
            </div>
        </div>
    </header>
);