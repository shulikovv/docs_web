import React from 'react';
import Pager from 'react-pager';
import Select from '../../atoms/Select';

export default class extends React.Component {
    constructor(props) {
		super(props);

		this.handlePageChanged = this.handlePageChanged.bind(this);
		this.handlePerPageChanged = this.handlePerPageChanged.bind(this);
		this.componentWillReceiveProps = this.componentWillReceiveProps.bind(this);

		this.state = {
			total:       props.total || 0,
			current:     props.current - 1 || 0,
            visiblePage: 5,
            perPage: props.perpage || 10
		};
	}

	handlePageChanged(newPage) {
        //console.log('newPage', newPage);
        this.props.onChange(newPage + 1);
		this.setState({ current : newPage });
    }
    
    handlePerPageChanged(newPerPage) {
        this.setState({perPage:newPerPage})
    }

    componentWillReceiveProps(nextProps) {
        if (this.state.total != nextProps.total) {
            this.setState({total: nextProps.total || 0});
        }
        this.setState({current:     nextProps.current - 1 || 0})
    }

    render() {
        console.log('pagination total', this.state.total);
        return(
            <div className="row" id="pagination">
                <div className='col-xs-4 col-md-4'>
                <Select
                    value={this.state.perPage}
                    multiple={false}
                    data={[10,25,50,100]}
                    placeholder=''
                    width='80px'
                    onChange={this.handlePerPageChanged}
                    className='initial-select-size'
                    disabled={true}
                    options={{
                        disabled:true
                    }}
                />
                <label className="margin-left-small">Entries Per Page</label>
                </div>

                <div className='col-xs-8 col-md-8 text-right'>
                {/*<div className="table-filter-message dataTables_info inline-block margin-right-small">Showing 10 entries</div>*/}
                <div className='table-pagination inline-block'>
                    <div className='dataTables_paginate pagination'>
                    <Pager
                        total={this.state.total}
                        current={this.state.current}
                        visiblePages={this.state.visiblePage}
                        titles={{ first: '<|', last: '>|' }}
                        className="pull-right"
                        onPageChanged={this.handlePageChanged}
                        />
                    </div>
                </div>
                </div>
            </div>
        )
    }
}