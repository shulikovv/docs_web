import Link from 'next/link';

export default (props) => (
    <div>
        { (props.document._source.pdf.langth>0) &&
            <span className="pull-right text-info"><i className="fa fa-download" aria-hidden="true"></i> Download PDF</span>
        }
        <a href={'doc/'+props.document._source.docId+props.document._source.filename}>
            <h2>
                <span className="text-info">{props.document._source.title}</span>
            </h2>
        </a>
        {props.document._source.chapter}
        <br/><br/>
        {props.document._source.content}
        <br/><br/>
        <span className="dolphin-grey-text">{props.document._source.filename}</span>
        <br/><br/>
        <div className='row'>
            <div className='col-xs-2 col-md-2'><span className='bold'>Product:</span> {props.document._source.product}</div>
            <div className='col-xs-2 col-md-2'><span className='bold'>Version:</span> {props.document._source.version}</div>
        </div>
        <hr/>
    </div>
)