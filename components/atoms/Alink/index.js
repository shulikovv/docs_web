import { withRouter } from 'next/router'

const ALink = ({ children, router, href, replace, m }) => {
  const style = {
    //marginRight: 10,
    //color: router.pathname === href? 'red' : 'black'
  }

  const handleClick = (e) => {
    e.preventDefault()
    console.log('click!')
    if (replace) {
        console.log('replace', href);
        router.replace('http://localhost:3000'+href)
    } else {
        router.push(href)
    }
  }

  return (
    <a href={href} onClick={handleClick} style={style}>
      {children}
    </a>
  )
}

export default withRouter(ALink)