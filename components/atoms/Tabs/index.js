import React from 'react';

export class Tabs extends React.Component {
    constructor(props) {
        super(props);
        this.displayName = 'Tabs';
        this.state = {
            selected: props.selected
        }
        this.shouldComponentUpdate = this.shouldComponentUpdate.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this._renderTitles = this._renderTitles.bind(this);
        this._renderContent = this._renderContent.bind(this);
    }

    shouldComponentUpdate(nextProps, nextState) {
        return this.props !== nextProps || this.state !== nextState;
    }

    handleClick (index, key, event) {
        event.preventDefault();
        this.setState({
            selected: index
        });
        this.props.onSelect(key);
    }

    _renderTitles () {
        function labels(child, index) {
          var activeClass = (this.state.selected === index ? 'active' : '');
          return (
            <li key={index} className={activeClass}>
              <a href="#" 
                onClick={this.handleClick.bind(this, index,child.key)}>
                {child.props.label}
            </a>
          </li>
        );
      }
        return (
          <ul className={"nom-tabs-large nom-tab-"+this.props.children.length}>
            {this.props.children.map(labels.bind(this))}
        </ul>
      );
    }

    _renderContent() {
        return (
          <div className="tabs__content">
              {this.props.children[this.state.selected]}
        </div>
      );
    }

    render () {
        return (
          <div className="nom-tab-panel">
          {this._renderTitles()}
            {this._renderContent()}
        </div>
      );
    }
}

Tabs.defaultProps = {
    selected: 0
};

export class Pane extends React.Component {
    constructor(props) {
        super(props);
        this.displayName = 'Pane';
    }
    render() {
        return (
            <div>
              {this.props.children}
          </div>
        );        
    }
}


/*
class extends React.Component {

    render() {
        return (
            <div>
            <Tabs selected={0} onSelect={(index)=>console.log('was selected',index)}>
              <Pane label="Tab 1" tkey='1' key='1'></Pane>
              <Pane label="Tab 2" key='2'></Pane>
              <Pane label="Tab 3" key='3'></Pane>
            </Tabs>
          </div>
        );        
    }
};
*/