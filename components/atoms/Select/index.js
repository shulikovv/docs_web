import React from 'react';
import Select2 from 'react-select2-wrapper';
import is from 'is_js';

export default class extends React.Component {
    render() {
        return (
            <Select2
                multiple={(typeof this.props.multiple !== 'undefined')?this.props.multiple:false}
                value = {this.props.value}
                data={this.props.data}
                ref={(e)=>{this.tag = e}}
                className={this.props.width?'initial-select-size':''}
                options={
                    {
                        placeholder: this.props.placeholder,
                        width: this.props.width,
                        disabled: typeof this.props.disabled == 'boolean' ?  this.props.disabled : false
                    }
                }
                onChange={(e)=>{
                    if (is.propertyDefined(this.tag,'el')) {
                        this.props.onChange(this.tag.el.val())
                    }
                }}
            />
        )
    }
}