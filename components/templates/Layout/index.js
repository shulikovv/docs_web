import Link from 'next/link';
import Head from 'next/head';

import Footer from '../../organisms/Footer';
import Breadcrumbs from '../../organisms/Breadcrumbs';
import Header from '../../organisms/Header';
import Navbar from '../../organisms/Navbar';

export default({
    children,
    title = 'NOMINUM - Documents'
}) => (
    <div>
        <Head>
            <title>{title}</title>
            <meta charSet='utf-8'/>
            <meta httpEquiv='X-UA-Compatible' content='IE=edge'/>
            <meta name='viewport' content='width=device-width, initial-scale=1'/>
            <meta name='defaultLanguage' content='en'/>
            <meta name='availableLanguages' content='en'/>
            <link rel="stylesheet" href="/static/application.css"/>
        </Head>
        <div id='topPageContainer' className='nom-container'>
            <Header></Header>
            <Navbar></Navbar>
        </div>

        {/*<Breadcrumbs />*/}

        <div className='nom-container'>
            {/*<div className="global-message">
                <div className="flash container">
                    <div
                        className="message alert alert-warning alert-dismissible"
                        role="alert"
                        data-id="c1167"
                        style={{
                        "display": "block"
                    }}>
                        <span className="close" data-dismiss="alert" aria-label="close">×</span>
                        You have User Agent updates available! Please check below for details.
                    </div>

                </div>
                </div>*/}
            <div id='mainContainer' className='container'>
                <div id='content' className='content'>
                    {children}
                </div>
            </div>
        </div>

        <Footer/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script
            src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script
            src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    </div>
);
