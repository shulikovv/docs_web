'use strict';

const Db = require('../components/db.elastic');
const db = new Db();

const is = require('is_js');
const path = require('path');
const htmlToText = require('html-to-text');
const {sayHelloInEnglish, getProducts, getContent} = require('../components/tools');

exports.index = function(req, res) {

}

exports.view = function(req,res) {
    let htmlPath = req.query.h.replace(/^\//,'');
    let docId = req.query.docId;
    getContent(docId,'/'+htmlPath).then(doc=>{
      res.status(200).json({'doc':doc});
    });
}