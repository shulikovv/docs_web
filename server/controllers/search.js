'use strict';

const Db = require('../components/db.elastic');
const db = new Db();

const is = require('is_js');
const path = require('path');
const htmlToText = require('html-to-text');

exports.index = function(req, res) {
    // test


    let offset = 0;
    if (req.query.page) {
      offset = (parseInt(req.query.page) - 1) * 10;
    }
    let doctype = req.query.doctype;
    let searchOptions = {};
    let query = req.query.query;
  
    if (doctype) {
      if (doctype == 'html') {
        query = query + ' AND ' + 'type:html';
      } else {
        query = query + ' AND ' + '-type:html';
      }
    } else {
      query = query + ' AND ' + 'type:html';
    }
  
    searchOptions = {
      index: 'nominum',
      size: 10,
      from: offset,
      body: {
        query: {
          bool: {
            must: {
              'query_string': {
                query: query
              }
            },
          }
        },
        'highlight': {
          'fields': {
            'content': {}
          }
        }
      },
    };
  
  
    if (is.array(req.query.products)) {
      searchOptions.body.query.bool.filter = {
        bool: {
          must: [
            {
              terms: {
                product: []
              }
            }
          ]
        }
      };
      req.query.products.forEach((element) => {
        searchOptions.body.query.bool.filter.bool.must[0].terms.product.push(element);
      });
    } else if (is.string(req.query.products) && req.query.products != 'all') {
      searchOptions.body.query.bool.filter.bool.must[0] = {
        terms: {
          product: [req.query.products.replace(/ /g, '_')]
        }
      };
    }
    if (is.propertyDefined(req.query, 'versions')) {
      if (is.propertyDefined(searchOptions.body.query.bool, 'filter')) {
        if (is.propertyDefined(searchOptions.body.query.bool.filter.bool, 'must')) {
          searchOptions.body.query.bool.filter.bool.must.push({
            terms: {
              version: req.query.versions
            }
          });
        }
      } else {
        searchOptions.body.query.bool.filter = {
          terms: {
            version: req.query.versions
          }
        };
      }
    }
  
  
    db.search(searchOptions).then(function (resp) {
      var hits = resp.hits.hits;
      let found = hits.map((item) => {
        let elem = Object.assign({}, item);
        elem._source.content = htmlToText.fromString(elem._source.content, {
          wordwrap: 130
        }).substring(0, 1024);
        if (is.propertyDefined(elem._source, 'content')) {
          elem._source.content = elem._source.content.substring(0, 1024);
        }
        elem._source.pdf = path.basename(elem._source.pdf_url || '');
        delete elem._source.raw;
        return elem;
      });
      res.status(200).json({
        total: resp.hits.total,
        data: found
      });
    }, function (err) {
      res.status(400).json(err);
    });

    //res.status(200).json(req.query);
}

exports.prodVer = function(req, res) {
   db.getProducts()
   .then(result => {
    res.status(200).json({prodVer:result});
   })
}