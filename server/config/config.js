module.exports = {
    flareDir: 'c:/Users/Vladyslav_Shulikov/nomidocfortest/html',
    res: [
      {
        type: 'html',
        module: 'flare',
        path: 'c:/Users/Vladyslav_Shulikov/nomidocfortest/html'
      },
      {
        type: 'pdf',
        module: 'pdf',
        path: '../docs-web-data/pdf'
      },
      {
        'type': 'docbook',
        'module': 'docbook',
        'path': ''
      },
      {
        'type': 'markdown',
        'module': 'markdown',
        'path': ''
      },
    ],
    log: {
        appenders: {
          out: {type: 'stdout'},
          err: {type: 'stderr'},
          stderr: {type: 'logLevelFilter', appender: 'err', level: 'error'},
          stdout: {type: 'logLevelFilter', appender: 'out', level: 'debug', maxLevel: 'warning'},
          server: {type: 'file', filename: './log/server.log'},
          watcher: {type: 'file', filename: './log/watcher.log'},
          all: {type: 'file', filename: './log/docs-web.log'}
        },
        categories: {
          default: {appenders: ['all', 'stderr', 'stdout'], level: 'debug'},
          server: {appenders: ['all', 'stderr', 'stdout', 'server'], level: 'debug'},
          watcher: {appenders: ['all', 'stderr', 'stdout', 'watcher'], level: 'debug'}
        }
      }
  };
  
  