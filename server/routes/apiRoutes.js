const express = require('express');
const router = express.Router();

const request = require('superagent');
const async = require('async');
const search = require('../controllers/search');
const doc = require('../controllers/doc');

router.get('/search', search.index);
router.get('/prod-ver', search.prodVer);
router.get('/doc', doc.view);

module.exports = router;
