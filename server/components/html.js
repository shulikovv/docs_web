const recursive = require('recursive-readdir');
const pathParse = require('path-parse');
const PATH = require('path');
const Promise = require('bluebird');
const is = require('is_js');
const fs = require('fs');
Promise.promisifyAll(fs);
const xmldoc = require('xmldoc');
const cheerio = require('cheerio');
const _ = require('lodash');
const mmh3 = require('murmurhash3');
const moment = require('moment');
const logger = require('log4js').getLogger('html');


function JsonWrongToRight(str) {
  return str
    .replace(/[^\x00-\x7F]/g, '')
    .replace(/(^define\(|\);$)/g, '')
    .replace(/:\s*"([^"]*)"/g, function (match, p1) {
      return ': "' + p1.replace(/:/g, '@colon@') + '"';
    })
    .replace(/:\s*'([^']*)'/g, function (match, p1) {
      return ': "' + p1.replace(/:/g, '@colon@') + '"';
    })
    .replace(/(['"])?([a-z0-9A-Z_\.\/\ \-]+)(['"])?\s*:/g, '"$2": ')
    .replace(/(['"])([a-z0-9A-Z_\.\/\ \#\-\?\(\)\,\\;]*)(['"])\s*/g, '"$2"')
    .replace(/\[\'\'\]/g, '[]')
    .replace(/\[\"\"\]/g, '[]')
    .replace(/@colon@/g, ':');
}


module.exports = class html {
  getDocuments(mainPath) {
    return new Promise((resolve) => {
      let ignoreFunc = (file, stats) => {
        return !(stats.isDirectory() || (PATH.basename(file) == 'StandardTitle.htm'));
      };
      recursive(mainPath, [ignoreFunc], (err, files) => {
        let out = [];
        if (is.array(files)) {
          files.forEach((file) => {
            let path = pathParse(file).dir.replace(/\\/g, '/');
            out.push(path.replace(mainPath.replace(/\/$/, '') + '/', '').replace('Content/Resources/Content Templates', ''));
          });
        }
        resolve(out);
      });
    });
  }

  getDocumentInfo(mainDir, docPath) {
    return new Promise((resolve) => {
      fs.readFileAsync(PATH.resolve(mainDir + '/' + docPath + '/Data/HelpSystem.xml'))
        .then((help) => {
          let document = new xmldoc.XmlDocument(help);
          fs.readFileAsync(PATH.resolve(mainDir + '/' + docPath + '/Content/Resources/Content Templates/StandardTitle.htm'))
            .then((title) => {
              const $ = cheerio.load(title);
              let _date = $('span.Manual.IdentificationManual.Date').text();
              try {
                _date = moment(_date, 'MMMM D, YYYY').format('YYYYMMDD');
              } catch (error) {
                _date = '';
              }
              let out = {
                'type': 'html',
                'status': 'new',
                'title': $('span.Manual.IdentificationManual.Name').text() || '',
                'chapter': '',
                'product': $('span.Manual.IdentificationProduct').text() || '',
                'version': $('span.Manual.IdentificationVersion').text() || '',
                'date': _date || '',
                'pathDoc': docPath,
                'pathToc': $('ul.menu._Skins_menu').attr('data-mc-linked-toc') || document.attr.Toc || '',
                'rootFile': document.attr.DefaultUrl || 'Content/index.thm',
                'url': ''
              };
              out.docId = out.product.replace(/ /g, '-') + '-' + out.version + '-' + out.title.replace(/ /g, '-') + '-' + out.date;
              logger.debug('Successfully got document info', out);
              resolve(out);
            });
        });
    });
  }

  async isDocument(mainDir, docPath) {
    let x = await this.getDocumentInfo(mainDir, docPath);
    return (is.not.empty(x.DefaultUrl) && is.not.empty(x.version) && is.not.empty(x.product));
  }

  async getTocTree(path, docPath) {
    let projectdata = await this.getDocumentInfo(path, docPath);
    let toc = await fs.readFileAsync(PATH.resolve(path + '/' + projectdata.pathDoc + '/' + projectdata.pathToc))
      .catch((e) => {
        logger.error('Error loading TOC file', e);
      });
    toc = toc.toString()
      .replace(/[^\x00-\x7F]/g, '')
      .replace(/(^define\(|\);$)/g, '')
      .replace(/\:\[\'/g, ':["')
      .replace(/\'\],/g, '"],')
      .replace(/{(i\:)/g, '{"i":')
      .replace(/:'(.*)',/g, ':"$1",')
      .replace(/([^a-z])(c\:)/g, '$1"c":')
      .replace(/([^a-z])(tree\:)/g, '$1"tree":')
      .replace(/([^a-z])(chunkstart\:)/g, '$1"chunkstart":')
      .replace(/([^a-z])(prefix\:)/g, '$1"prefix":')
      .replace(/([^a-z])(numchunks\:)/g, '$1"numchunks":')
      .replace(/([^a-z])(n\:\[)/g, '$1"n":[')
      .replace(/([^a-z])t\:\[/g, '$1"t":[');

    return JSON.parse(toc);
  }


  flatTocList(toc) {

  }

  async getTocItemList(path, docPath, projectdata) {
    if (!projectdata) {
      projectdata = await this.getDocumentInfo(path, docPath);
    }
    let toc = await fs.readFileAsync(PATH.resolve(path + '/' + projectdata.pathDoc + '/' + projectdata.pathToc))
      .catch((e) => {
        logger.error('Error loading TOC items', e);
      });
    toc = JsonWrongToRight(toc.toString().replace(/[^\x00-\x7F]/g, ''));
    return JSON.parse(toc);
  }

  async getDocumentContent_(path, docPath) {
    let projectdata = await this.getDocumentInfo(path, docPath);
    //let tocItems = await this.getTocItemList(path, docPath, projectdata);
    let Toc = projectdata.pathToc.replace('.js', '_Chunk0.js');
    logger.debug('Reading TOC file', PATH.resolve(path + '/' + projectdata.pathDoc + '/' + Toc));
    let toc = await fs.readFileAsync(PATH.resolve(path + '/' + projectdata.pathDoc + '/' + Toc)).catch((e) => {
      logger.error('Error loading document contents', e, path, projectdata.pathDoc, Toc);
    });
    toc = JsonWrongToRight(toc.toString().replace(/[^\x00-\x7F]/g, ''));
    let nn = {};
    try {
      nn = JSON.parse(toc);
    } catch (e) {
      nn = {};
      logger.debug(e,toc);
    }

    let out = [];
    for (let key in nn) {
      out.push(Object.assign({}, nn[key], {href: key}));
    }
    /*
    let out2 = [];
    tocItems.tree.n.forEach(element => {  
      let idx = _.findIndex(out, function (o) {
        return is.inArray(element.i, o.i);
      });
      let i = out[idx].i.indexOf(element.i);
      out2.push(Object.assign({}, out[idx], {ii: element.i, title: out[idx].t[i]}));
    });
    */
    nn = Object.assign([], out);
    //console.log('=======',nn);

    let nn2 = [];
    nn.forEach((item) => {
      if (fs.existsSync(PATH.resolve(path + '/' + projectdata.pathDoc + '/' + item.href))) {
        nn2.push(item);
      }
    });

    out = await Promise.map(nn2, async function (item, idx) {
      let content = await fs.readFileAsync(PATH.resolve(path + '/' + projectdata.pathDoc + '/' + item.href)).catch(() => {
      });
      let $ = cheerio.load(content.toString());
      $('body section.main-section .nocontent').remove();
      $('body').html().replace(/(?:\r\n|\r|\n)/g, '');
      $('body section.main-section .nocontent').remove();
      $('script').remove();
      let html;
      try {
        html = $('body section.main-section').html().replace(/(?:\r\n|\r|\n)/g, '');
      } catch (error) {
        // TODO: add loger
      }
      content = $('body section.main-section').text().replace(/[^\x00-\x7F]/g, '');

      let title = $('h1.ChapterTitle').text() || $('h1.SectionTitle').text() || $('p.heading1').text() || '';

      return {
        id: idx,
        guid: mmh3.murmur128HexSync(projectdata.product + projectdata.version + projectdata.title + item.href),
        type: 'html',
        title: projectdata.title,
        chapter: title,
        product: projectdata.product,
        version: projectdata.version,
        date: '',
        docId: projectdata.docId,
        url: '',
        pathDoc: projectdata.pathDoc,
        pathToc: projectdata.pathToc,
        rootfile: projectdata.rootFile,
        filename: item.href,
        urlhash: '',
        content: content,
        html: html,
        contentHash: mmh3.murmur128HexSync(content),
      };
    });
    return out;
  }

  getDocumentContent(path, projectdata) {

    let Toc = projectdata.Toc.replace('.js', '_Chunk0.js');
    if (!fs.existsSync(path + projectdata.dirname + '/' + Toc)) {
      return false;
    }
    fs.readFileAsync(path + projectdata.dirname + '/' + Toc)
      .then((toc) => {
        toc = toc.toString()
          .replace(/(^define\(|\);$)/g, '')
          .replace(/\'/g, '"')
          .replace(/\//g, '\\/')
          .replace(/b\:\[/g, '"b":[')
          .replace(/i\:\[/g, '"i":[')
          .replace(/([^a-z])(n\:\[)/g, '$1"n":[')
          .replace(/([^a-z])t\:\[/g, '$1"t":[');
        let nn;
        eval('nn = ' + toc);
        let out = [];
        let files = [];
        for (var key in nn) {
          files.push({
            t: [nn[key].t[0]],
            href: key
          });
          if (nn[key].i.length > 0) {
            nn[key].i.forEach((item, i) => {
              out.push({
                'i': item,
                't': nn[key].t[i],
                'b': nn[key].b[i],
                'href': key
              });
            });
          }
        }
        nn = _.orderBy(out, 'i', ['asc']);

      })
      .catch(() => {

      });
  }

  async getDocumentTOC(path, docPath) {
    let projectdata = await this.getDocumentInfo(path, docPath);
    let Toc = projectdata.Toc.replace('.js', '_Chunk0.js');
    let toc = await fs.readFileAsync(path + projectdata.dirname + '/' + Toc);
    toc = toc.toString()
      .replace(/(^define\(|\);$)/g, '')
      .replace(/\'/g, '"')
      .replace(/\//g, '\\/')
      .replace(/b\:\[/g, '"b":[')
      .replace(/i\:\[/g, '"i":[')
      .replace(/([^a-z])(n\:\[)/g, '$1"n":[')
      .replace(/([^a-z])t\:\[/g, '$1"t":[');
    let nn;
    eval('nn = ' + toc);
    let out = [];
    let files = [];
    for (var key in nn) {
      files.push({
        t: [nn[key].t[0]],
        href: key
      });
      if (nn[key].i.length > 0) {
        nn[key].i.forEach((item, i) => {
          out.push({
            'i': item,
            't': nn[key].t[i],
            'b': nn[key].b[i],
            'href': key
          });
        });
      }
    }
    nn = _.orderBy(out, 'i', ['asc']);
    return nn;
  }

  getDocumentUrl() {

  }
};