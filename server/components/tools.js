'use strict';

const Db = require('../components/db.elastic');
const db = new Db();
const is = require('is_js');
const _ = require('lodash');
const cheerio = require('cheerio');
const path = require('path');
const {loaderTOC} = require('./tocLoader');
const config = require('../config/config');
const fs = require('fs');
const htmlToJsx = require('html-to-jsx');

var exports = module.exports = {};

exports.JsonWrongToRight = function (str) {
    return str
      .replace(/[^\x00-\x7F]/g, '')
      .replace(/(^define\(|\);$)/g, '')
      .replace(/:\s*"([^"]*)"/g, function (match, p1) {
        return ': "' + p1.replace(/:/g, '@colon@') + '"';
      })
      .replace(/:\s*'([^']*)'/g, function (match, p1) {
        return ': "' + p1.replace(/:/g, '@colon@') + '"';
      })
      .replace(/(['"])?([a-z0-9A-Z_\.\/\ \-]+)(['"])?\s*:/g, '"$2": ')
      .replace(/(['"])([a-z0-9A-Z_\.\/\ \#\-\?\(\)\,\\;]*)(['"])\s*/g, '"$2"')
      .replace(/\[\'\'\]/g, '[]')
      .replace(/\[\"\"\]/g, '[]')
      .replace(/@colon@/g, ':');
}

exports.getMenu = function(menu, toc2) {
    let _out = [];
    menu.forEach(element => {
        let indx = _.findIndex(toc2,function(o) { return is.inArray(element.i,o.i); });
        let idx = _.indexOf(toc2[indx].i,element.i);
        let _objTmp = {
            i: element.i,
            c: element.c,
            t: toc2[indx].t[idx],
            b: toc2[indx].b[idx] || '',
            h: toc2[indx].href,
        };
        if (is.array(element.n)) {
           _objTmp.n = exports.getMenu(element.n, toc2)
        }
        _out.push(_objTmp);
    });
    return _out;
}

exports.getContent = function(docId,filename) {

    let rootPath = config.flareDir;

    

    return new Promise((resolve, reject) => {
        console.log('>>>>>>',docId,filename);
        db.searchByIdfilename(docId,filename).then(doc=>{
            function getMenu(menu, toc2) {
                let _out = [];
                menu.forEach(element => {
                    let indx = _.findIndex(toc2,function(o) { return is.inArray(element.i,o.i); });
                    let idx = _.indexOf(toc2[indx].i,element.i);
                    let _objTmp = {
                        i: element.i,
                        c: element.c,
                        t: toc2[indx].t[idx],
                        b: toc2[indx].b[idx] || '',
                        h: toc2[indx].href,
                    };
                    if (is.array(element.n)) {
                       _objTmp.n = getMenu(element.n, toc2)
                    }
                    _out.push(_objTmp);
                });
                return _out;
            }
            let pathToToc1 = doc.hits.hits[0]._source.pathToc;
            let pathToToc2 = pathToToc1.replace(/.js$/,'_Chunk0.js');
            let pathDoc = doc.hits.hits[0]._source.pathDoc;
            let cont1 = fs.readFileSync(rootPath+'/'+pathDoc+'/'+pathToToc1);
            let toc1 = JSON.parse(exports.JsonWrongToRight(cont1.toString().replace(/[^\x00-\x7F]/g, '')));
            let cont2 = fs.readFileSync(rootPath+'/'+pathDoc+'/'+pathToToc2);
            let toc2Obj = JSON.parse(exports.JsonWrongToRight(cont2.toString().replace(/[^\x00-\x7F]/g, '')));
            let toc2 = [];
            for ( let key in toc2Obj) {
                    toc2.push({...toc2Obj[key],href:key});
                }
            const url = path.join(rootPath, pathDoc, decodeURIComponent(filename.replace(/(\#|\?)(.)*/g, '')));
            let _htmlContent = fs.readFileSync(path.resolve(url), 'utf8');
            let $ = cheerio.load(_htmlContent.toString());
            //$('body section.main-section .nocontent').remove();
            //$('body').html().replace(/(?:\r\n|\r|\n)/g, '');
            $('body section.main-section .nocontent').remove();
            $('script').remove();
            $ = cheerio.load($('body section.main-section').html().toString());
            $('img').each(function(){
                let href = $(this).attr('src');
                if (href) {
                  if (href.match(/^\#/)) {
                    $(this).attr('src', projectPath + href);
                  } else {
                    let dir = path.dirname(filename);
                    let resolvedpath = path.resolve('/'+pathDoc+'/'+dir, href).replace(/^C\:/, '').replace(/\\/g, '/');
                    //logger.debug('img src parse',href,'|',dir,'|',projectPath,'||',resolvedpath);
                    $(this).attr('src', resolvedpath.replace(rootPath, '/'));
                  }
                }                
            })
            console.log('--------------------------------------------',doc,$);
            resolve({
                menu: getMenu(toc1.tree.n, toc2),
                html: $.html(),
                source: doc.hits.hits[0]._source,
                pathDoc: doc.hits.hits[0]._source.pathDoc,
                pathToc: doc.hits.hits[0]._source.pathToc,
            });
        })
    })
    
    
    //let cont1 = fs.readFileSync('/home/vlad/nominum/data/data2/dfg/Data/Tocs/engage_manual.js');
    //let cont1 = fs.readFileSync(pathToHtml+pathToPrj+pathToToc1);
    //let toc1 = JSON.parse(JsonWrongToRight(cont1.toString().replace(/[^\x00-\x7F]/g, '')));

    //let cont2 = fs.readFileSync('/home/vlad/nominum/data/data2/dfg/Data/Tocs/engage_manual_Chunk0.js');
    //let cont2 = fs.readFileSync(pathToHtml+pathToPrj+pathToToc2);
    //let toc2Obj = JSON.parse(JsonWrongToRight(cont2.toString().replace(/[^\x00-\x7F]/g, '')));
    //let toc2 = [];
    //for ( let key in toc2Obj) {
    //    console.log(key);
    //    toc2.push({...toc2Obj[key],href:key});
    //}
}

exports.sayHelloInEnglish = function(docid) {
    let rootPath = config.flareDir;
    db.search({
        index: 'nominum',
        _sourceInclude: ['docId', 'pathDoc', 'pathToc'],
        body: {
        query: {
            term: {
            docId:docid
            }
        }
        }
    })
    .then((resp) => {
    var hits = resp.hits.hits;
    let documentInfo = {};
    if (is.array(hits)) {
        documentInfo = hits[0]._source;
    }
    let projectPath = documentInfo.pathDoc;
    let toc = documentInfo.pathToc;
    let toc2 = toc.replace('.js', '_Chunk0.js');
    let pathToToc = path.join(rootPath, projectPath);
    loaderTOC(path.join(pathToToc, toc), path.join(pathToToc, toc2))
        .then((result) => {
            return{
                menu: result
            };
        });
    })
    .catch((err) => {
        //logger.error(err);
        return {
            err: err
        };
    });
};

exports.getProducts = function() {
    return db.getProducts();
}