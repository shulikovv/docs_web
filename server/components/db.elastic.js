const is = require('is_js');
const _ = require('lodash');
const Promise = require('bluebird');

var elasticsearch = require('elasticsearch');
var bodybuilder = require('bodybuilder');

let instance = null;

module.exports = class dbElastic {

    static getClient() {
      if (instance) {
        return instance.client;
      } else {
        let db = new dbElastic();
        return db.client;
      }
    }
  
    constructor ()
    {
      if (!instance) {
        instance = this;
      }
      this._type = 'dbElastic';
      this.time = new Date();
      this.client = new elasticsearch.Client({
        host: 'localhost:9200',
        //log: 'trace' //process.env.DEBUG ? 'trace' : 'warning'
      }); 
      return instance;
    }

    search(searchOptions) {
        return this.client.search(searchOptions);
    }

    searchById(docId) {
      let body = bodybuilder().filter('term', 'docId', docId).filter('term', 'type', 'html');
      return this.client.search({
        index: 'nominum',
        body: body.build()
      });
    }
    
    searchByIdfilename(docId, filename) {
      let body = bodybuilder().filter('term', 'docId', docId).filter('term', 'filename', filename).filter('term', 'type', 'html');
      return this.client.search({
        index: 'nominum',
        body: body.build()
      });
    }

    getProducts() {
      let self = this;
      return new Promise(function(resolve,reject){
        let body = bodybuilder().aggregation('terms', 'product',{
          order: { _term: 'desc' },
          size: 10000
        }, 
            agg => agg.aggregation('terms', 'version',{
              order: { _term: 'desc' },
              size: 10000
            })).build();
        self.client.search({
          index: 'nominum',
          type: 'type1',
          _sourceExclude: ['content', 'html', 'raw'],
          size: 0,
          body:body
        }).then(function (resp) {
          let out = {};
          resp.aggregations.agg_terms_product.buckets.forEach(item => {
            let versions = item.agg_terms_version.buckets.map((_version)=>{
              return _version.key;
            })
            out[item.key]=versions;
          });
          resolve(out);
        }, function (err) {
          reject(err.message);
        });
      });
    }

}  