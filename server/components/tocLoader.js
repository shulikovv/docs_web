const path = require('path');
const _ = require('lodash');
const Promise = require('bluebird');
const is = require('is_js');
const fs = require('fs');
const {JsonWrongToRight} = require('./tools');

Promise.promisifyAll(fs);

var exports = module.exports = {};

exports.loaderTOCNext = function(rootPath2, rootPath) {
    return new Promise((resolve) => {
        fs.readFileAsync(path.resolve(rootPath2), 'utf8')
          .then((doc) => {
            var code4 = JsonWrongToRight(doc.toString());
            var nn0 = JSON.parse(code4);
            fs.readFileAsync(rootPath, 'utf8')
              .then((doc) => {
                var code4 = JsonWrongToRight(doc.toString())
                var nn1 = JSON.parse(code4);
                for (var key in nn1) {
                  nn1[key] = Object.assign(nn1[key], {href: key});
                }
                nn1 = _.orderBy(nn1, ['i'], ['asc']);
                let out = [];
                toAr(nn0.tree.children, out, 0);
                out.forEach((item, index) => {
                  let idx = _.findIndex(nn1, {i: [item.i]});
                  if (idx > -1) {
                    let tIdx = _.indexOf(nn1[idx].i, item.i);
                    out[index].t = nn1[idx].t[tIdx];
                    out[index].href = nn1[idx].href + nn1[idx].b[tIdx];
                  }
                });
                resolve(out);
              });
          });
      });
}

exports.loaderTOC = function(rootPath2, rootPath) {
  function toAr(children, out = [], level = 0) {
    let _level = level + 1;

    children.forEach((el) => {
      out.push({
        i: el.i,
        level: _level
      });
      if (is.propertyDefined(el, 'children')) {
        toAr(el.children, out, _level);
      }
    });
  }

  return new Promise((resolve) => {
    fs.readFileAsync(path.resolve(rootPath2), 'utf8')
      .then((doc) => {
        var code4 = doc.toString()
          .replace(/(^define\(|\);$)/g, '')
          .replace(/\'/g, '"')
          .replace(/\//g, '\\/')
          .replace(/(\{|\,)(\w):/g, '$1"$2":')
          .replace(/\"n\"/g, '"children"');
        var nn0 = eval(`(${code4})`);
        fs.readFileAsync(rootPath, 'utf8')
          .then((doc) => {
            var code4 = doc.toString()
              .replace(/(^define\(|\);$)/g, '')
              .replace(/\'/g, '"')
              .replace(/\//g, '\\/')
              .replace(/b\:\[/g, '"b":[')
              .replace(/i\:\[/g, '"i":[')
              .replace(/([^a-z])(n\:\[)/g, '$1"n":[')
              .replace(/([^a-z])t\:\[/g, '$1"t":[');
            var nn1 = eval(`(${code4})`);
            for (var key in nn1) {
              nn1[key] = Object.assign(nn1[key], {href: key});
            }
            nn1 = _.orderBy(nn1, ['i'], ['asc']);
            let out = [];
            toAr(nn0.tree.children, out, 0);
            out.forEach((item, index) => {
              let idx = _.findIndex(nn1, {i: [item.i]});
              if (idx > -1) {
                let tIdx = _.indexOf(nn1[idx].i, item.i);
                out[index].t = nn1[idx].t[tIdx];
                out[index].href = nn1[idx].href + nn1[idx].b[tIdx];
              }
            });
            resolve(out);
          });
      });
  });
};