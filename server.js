const compression = require('compression');
const express = require('express');
const bodyParser = require('body-parser');
const session = require('express-session');
const path = require('path');

const dev = process.env.NODE_ENV !== 'production';
const next = require('next');
const pathMatch = require('path-match');
const app = next({ dev });
const handle = app.getRequestHandler();
const { parse } = require('url');
const config = require('./server/config/config');

const apiRoutes = require('./server/routes/apiRoutes.js');
const {sayHelloInEnglish, getProducts, getContent} = require('./server/components/tools');

app.prepare().then(() => {
  const server = express();
  server.use(compression());
  server.use(bodyParser.json());
  server.use(session({
    secret: 'super-secret-key',
    resave: false,
    saveUninitialized: false,
    cookie: { maxAge: 60000 }
  }));

  server.use('/api', apiRoutes);

  // Server-side
  const route = pathMatch();

  server.get('/', (req, res) => {
    let dfg = sayHelloInEnglish();
    getProducts()
      .then(prodVer => {
        return app.render(req, res, '/', {'prodVer': prodVer, ...req.query});
      })
  });


  server.get('/search', (req, res) => {
    let dfg = sayHelloInEnglish();
    getProducts()
      .then(prodVer => {
        return app.render(req, res, '/search', {'prodVer': prodVer, ...req.query});
      })
  });

  server.get('/doc', (req, res) => {
    console.log('doc',req.query);
    let htmlPath = req.query.h.replace(/^\//,'');
    let docId = req.query.docId;
    getContent(docId,'/'+htmlPath).then(doc=>{
      console.log('>>>>>11',doc);
      return app.render(req, res, '/doc', {'doc':doc});
    });
  });


  server.get('/doc/:docId/*', (req, res) => {
    console.log('doc');
    let htmlPath = req.params[0];
    let docId = req.params.docId;
    getContent(docId,'/'+htmlPath).then(doc=>{
      return app.render(req, res, '/doc', {'doc':doc});
    });
  });

  server.get('/admin/html', (req, res) => {
    return app.render(req, res, '/admin/html', req.query);
  });
  
  server.use(
    express.static(config.flareDir)
  );

  server.get('*', (req, res) => {
    return handle(req, res);
  });


  /* eslint-disable no-console */
  server.listen(3000, (err) => {
    if (err) throw err;
    console.log('Server ready on http://localhost:3000');
  });
});
